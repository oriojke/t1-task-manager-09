package ru.t1.didyk.taskmanager.api;

import ru.t1.didyk.taskmanager.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
