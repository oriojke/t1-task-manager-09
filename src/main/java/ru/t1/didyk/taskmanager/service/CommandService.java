package ru.t1.didyk.taskmanager.service;

import ru.t1.didyk.taskmanager.api.ICommandRepository;
import ru.t1.didyk.taskmanager.api.ICommandService;
import ru.t1.didyk.taskmanager.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
