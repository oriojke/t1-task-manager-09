package ru.t1.didyk.taskmanager;

import ru.t1.didyk.taskmanager.context.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
