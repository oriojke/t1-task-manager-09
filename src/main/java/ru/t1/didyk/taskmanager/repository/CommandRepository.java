package ru.t1.didyk.taskmanager.repository;

import ru.t1.didyk.taskmanager.api.ICommandRepository;
import ru.t1.didyk.taskmanager.constant.ArgumentConst;
import ru.t1.didyk.taskmanager.constant.CommandConst;
import ru.t1.didyk.taskmanager.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO,
            "Show system info."
    );

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    private static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP,
            "Show command list."
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT, null,
            "Close application."
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );

    private static final Command[] COMMANDS = new Command[]{
            INFO, ABOUT, HELP, EXIT, VERSION
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
